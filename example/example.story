# Friend

## visit friend

Outside you visit a friend.

## play video games

You play video games with your friend.
It's much fun.

## talk friend

You talk a bit with your friend.
It's very interesting to share your political beliefs.

## leave friend

After you did some cool things with your friend, you leave again.

## play outside

You both go outside again and walk around.
It's relaxing outside when a person you like is on your side.

# Home

## stay home

You stay at home.
You have a nice day with your mother.

## leave home

You leave your home.

## intro

You live at home with your mother.

## mothers story

Your mother stays at home alone.
While you're not there, she invites friends to have a huge party.

# Outside

## Adventure

### meet girl

In the city, you meet an interesting girl.
She wants to have some adventure with you.
You join her and leave the city together.
There's an interesting forest near the city.
She shows you an old building, but you need two keys to enter it.
You decide to spilt, so each of you can find one of the keys.

### adventure together

You both meet again with the keys and enter the building, where the real adventure begins.

### your adventure

You look for a key and find it.

### girl adventure

The girl looks for a key and finds it.

## City

### visit aunt

You visit your uncle.
You have a fun time with him and leave again.

### visit uncle

You go to your aunt.
She's a bit weird, but can tell you interesting things about the city, before you leave again.

### visit father

You visit your father.
You missed him the most.
You spend the night with him, but go outside again the next day.

## Journey

### go cave

You enter the dark cave.
It's kind of scary there.
But after some time it gets brighter because of burning torches hanging around.
You go further and further, until you see a bright light. The exit is near.
Soon you leave the cave again, eager to see, what's on the other side.

### go city

You see a city, so you enter it.
It's the city where the rest of your familiy lives: Your uncle, your aunt and your father.
You are looking forward to see them all again.

### go away

You go far away from your home.
There you see a small village, a wide field and a dark cave.

### go forest

You go to the forest.
It's pretty dark there, but you like it.

### go field

You go to the field.
There you run around until you see an interesting looking forest.

### go village

You enter the village.
You can see many cool people there.
One of them tells you about a forest, where the villagers go to relax, so you think about going there as well.

